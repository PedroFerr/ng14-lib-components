import { NgModule } from '@angular/core';
import { MyLibComponent } from './my-lib.component';

import { EllipsisModule } from './components/ellipsis/ellipsis.module';
import { SpinnerModule } from './components/spinner/spinner.module';

import { LoadingService } from './services/loading.service';

@NgModule({
    declarations: [
        MyLibComponent,
    ],
    imports: [],
    exports: [
        MyLibComponent,
        EllipsisModule,
        SpinnerModule,
    ],
    providers: [LoadingService]
})
export class MyLibModule { }
