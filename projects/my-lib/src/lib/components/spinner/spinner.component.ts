import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../../services/loading.service';

@Component({
    selector: 'lib-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
    loading$ = this.loader.loading$;

    constructor(
        public loader: LoadingService,
    ) { }

    ngOnInit() {
    }

}
