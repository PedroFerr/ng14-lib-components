import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatButtonModule } from '@angular/material/button'

import { SpinnerComponent } from './spinner.component';

@NgModule({
    declarations: [SpinnerComponent],
    imports: [
        CommonModule,
        MatProgressSpinnerModule,
        MatButtonModule,
    ],
    exports: [SpinnerComponent]
})
export class SpinnerModule { }
